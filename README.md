# Requisitos para rodar os ambientes

1. Backend
 - banco de dados postgres
 - criar uma base de dados de nome "mercado"
 - criar um schmema com o nome "mercado"
2. Frontend
 - instalar node 6+
 - a aplicação não está utilizando as telas junto com o projeto no backend
 - está sendo necessário instalar o plugin "Allow-Control-Allow-Origin" no navegador
 