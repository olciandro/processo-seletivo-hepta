import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'bootstrap/dist/css/bootstrap.css';
import 'axios/dist/axios.js';
import jQuery from 'popper.js';
import VueResource from 'vue-resource';
import 'vue-material/dist/vue-material.js';

Vue.use(VueResource);

window.$ = window.jQuery = jQuery;
import 'jquery';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
