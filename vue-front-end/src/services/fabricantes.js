import {http} from './config';

export default {
    listar: () => {
        return http.get('fabricantes')
    },

    salvar: (fabricante) => {
        return http.post('fabricante', fabricante)
    },

    atualizar: (fabricante) => {
        return http.put('fabricante', fabricante)
    },

    deletar: (id) => {
        return http.delete('id', {data: fabricante.id})
    }
}