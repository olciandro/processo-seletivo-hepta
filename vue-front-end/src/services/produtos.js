import {http} from './config';

export default {
    listar: () => {
        return http.get('produtos');
    },

    salvar: (produto) => {
        return http.post('produto', produto);
    },

    atualizar: (produto, id) => {
        return http.put('produto/id', produto, id);
    },

    deletar: (id) => {
        return http.delete('id', {data: produto.id});
    }
}