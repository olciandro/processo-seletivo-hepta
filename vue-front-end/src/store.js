import Vue from "vue";
import Vuex from "vuex";
import BootstrapVue from 'bootstrap-vue';

Vue.use(Vuex);
Vue.use(BootstrapVue);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {}
});
